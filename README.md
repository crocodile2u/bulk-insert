# bulk-insert

Class for building bulk-insert queries into SQL databases.

Usage example:

```php
$pdo = new \PDO();
$userlist = [
    [1, "John"],
    [2, "Paul"],
    [3, "George"],
    [4, "Ringo"],
];
$sql = "INSERT INTO users (id, name) VALUES";
$template = "(?, ?)";
$buffer = new \BulkInsert\Buffer($sql, $template, 3);
foreach ($userlist as $user) {
    if ($executable = $buffer->append($user)) {
        // INSERT INTO users (id, name) VALUES (?, ?), (?, ?), (?, ?)
        $stmt = $pdo->prepare($executable->sql());
        // [1, "John", 2, "Paul", 3, "George"]
        $stmt->execute($executable->bindings());
    }
}
// insert what still remains in the buffer
if ($buffer->isExecutable()) {
    // INSERT INTO users (id, name) VALUES (?, ?)
    $stmt = $pdo->prepare($buffer->sql());
    // [4, "Ringo"]
    $stmt->execute($buffer->bindings());
}
```