<?php
declare(strict_types=1);

namespace BulkInsert;

class Buffer
{
    const DEFAULT_LIMIT = 1000;
    private $rowCount = 0;
    private string $sql;
    private array $bindings = [];
    private int $limit;
    private string $template;
    private int $rowSize;

    /**
     * Buffer constructor.
     * @param string $sql
     */
    public function __construct(string $sql, string $template, int $limit = self::DEFAULT_LIMIT)
    {
        $this->sql = $sql;
        $this->template = $template;
        $this->rowSize = substr_count($template, "?");
        $this->limit = $limit;
    }

    public function isExecutable(): bool
    {
        return $this->rowCount > 0;
    }

    public function sql(): string
    {
        return $this->sql . " " . join(", ", array_fill(0, $this->rowCount, $this->template));
    }

    public function bindings(): array
    {
        return $this->bindings;
    }

    /**
     * @param array $row
     * @return $this|null
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function append(array $row): ?self
    {
        if (count($row) !== $this->rowSize) {
            throw new \InvalidArgumentException(
                sprintf("every row must have exactly %d values, got %s", $this->rowSize, json_encode($row))
            );
        }
        $this->rowCount++;
        if ($this->rowCount > $this->limit) {
            $message = sprintf(
                "buffer overflow: max %d rows allowed, have you run append() on executable buffer?",
                $this->limit
            );
            throw new \LogicException($message);
        }
        $this->bindings = array_merge($this->bindings, $row);
        if ($this->rowCount === $this->limit) {
            $ret = clone $this;
            $this->rowCount = 0;
            $this->bindings = [];
            return $ret;
        }
        return null;
    }
}