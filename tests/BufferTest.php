<?php

namespace Tests;

use BulkInsert\Buffer;
use PHPUnit\Framework\TestCase;

class BufferTest extends TestCase
{
    public function testHappyFlow()
    {
        $sql = "INSERT INTO users (id, name) VALUES";
        $template = "(?, ?)";
        $buffer = new Buffer($sql, $template, 3);

        $users = [
            [1, "John"],
            [2, "Paul"],
            [3, "George"],
            [4, "Ringo"],
        ];

        // adding first 2 rows shouldn't result in anything
        $this->assertNull($buffer->append($users[0]));
        $this->assertNull($buffer->append($users[1]));

        // third row returns an executable buffer
        $executable = $buffer->append($users[2]);
        $this->assertInstanceOf(Buffer::class, $executable);
        $this->assertTrue($executable->isExecutable());
        $this->assertEquals("INSERT INTO users (id, name) VALUES (?, ?), (?, ?), (?, ?)", $executable->sql());
        $this->assertEquals(array_merge($users[0], $users[1], $users[2]), $executable->bindings());

        // now the original buffer should be reset
        $this->assertFalse($buffer->isExecutable());

        $this->assertNull($buffer->append($users[3]));
        $this->assertTrue($buffer->isExecutable());
        $this->assertEquals("INSERT INTO users (id, name) VALUES (?, ?)", $buffer->sql());
        $this->assertEquals($users[3], $buffer->bindings());
    }

    public function testInvalidRowSize()
    {
        $this->expectException(\InvalidArgumentException::class);

        $sql = "INSERT INTO users (id, name) VALUES";
        $template = "(?, ?)";
        $buffer = new Buffer($sql, $template);
        $buffer->append(["x", "y", "z"]);
    }

    public function testOverflow()
    {
        $this->expectException(\LogicException::class);

        $sql = "INSERT INTO users (id, name) VALUES";
        $template = "(?, ?)";
        $buffer = new Buffer($sql, $template, 1);
        $executable = $buffer->append([1, "John"]);
        $executable->append([2, "Paul"]);
    }
}
